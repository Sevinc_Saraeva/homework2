import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Homework2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        char ar[] [] =new char[5][5];
        ar = fillArray(ar);
        Random r = new Random();
   // to generate random target
        int cor_vertical = r.nextInt((5 - 0) + 1) + 0;
        int cor_horizontal = r.nextInt((5 - 0) + 1) + 0;
        System.out.println("All set. Get ready to rumble!");
       boolean flag = false;
       while(!flag){
           printArray(ar);
           // to ask player enter the shooting target
           int line = getNumber("line");
           int  bar = getNumber("bar");
             flag = shootTarget(cor_vertical, cor_horizontal, ar, line, bar);

       }
    }
    //to fill array with char ' - '
    public static char[][] fillArray(char[][] ar) {
        for (int i = 0; i < ar.length; i++) {
            for (int j = 0; j < ar.length; j++) {
                ar[i][j] = '-';
            }
        }
        return ar;
    }
   // to get number from player and check if it is number and in a range of  0 and 5
    public static int getNumber(String text) {
        int number=-1;
        Scanner in = new Scanner(System.in);
        boolean flag = true;
        while (flag == true) {
            System.out.println("Please enter a  " + text + " in a range of (1-5)");
            String num = in.next();
            boolean boo = true;
            for (int i = 0; i < num.length(); i++) {
                if (num.charAt(i) < 48 || num.charAt(i) > 57) {
                    boo = false;
                    break;
                     }
            }
            if (boo == true) {
                number = Integer.parseInt(num);
                if (number <=0 || number > 5){
                    flag = true;
            }
                else flag = false;
        }}
        return number-1;
    }
       // to check id player shoot the target
        public static boolean checkTarget( int target_vertical, int target_horizontal, int line, int bar){
       if(target_horizontal==line && target_vertical == bar) return true;
       return  false;
        }
  // to check if player has already shoot there
        public static boolean checkAlreadyShoot(char[][]ar, int line, int bar){
        if(ar[line][bar] == '*') return true;
        return false;
        }
          // to shoot target
        public static boolean shootTarget(int target_vertical, int target_horizontal,char[][]ar, int line, int bar){
        if(checkTarget(target_vertical, target_horizontal, line, bar)== false){
            if(checkAlreadyShoot(ar, line, bar) == false){
                ar[line][bar] = '*';
            }
            else System.out.println("You have already shot here");
            return false;
        }
        else{
            ar[line][bar] = 'x';
            System.out.println("You have won!");

        }
            printArray(ar);
            return true;
        }

          // to print the table
        public  static void printArray(char[][]ar){

            for (int i = 0; i < ar.length; i++) {
                for (int j = 0; j < ar.length; j++) {
                    System.out.print(ar[i][j] + " ");
                }
                System.out.println();
            }


        }
}
